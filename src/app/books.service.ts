import { Injectable } from '@angular/core';

@Injectable()
export class BooksService {

  constructor() { };

  getBooks(): Promise<any[]> {
    return Promise.resolve([
      { id: '01', name: 'Clean Code: A Handbook of Agile Software Craftsmanship'},
      { id: '02', name: 'Agile Principles, Patterns, and Practices in C#'},
      { id: '03', name: 'Practices of an Agile Developer: Working in the Real World'},
      { id: '04', name: 'Refactoring: Improving the Design of Existing Code'},
      { id: '05', name: 'Design Patterns: Elements of Reusable Object-Oriented Software'}
    ]);
  }
}
