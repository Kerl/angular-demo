import { Component } from '@angular/core';
import { BooksService } from 'app/books.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [BooksService]
})
export class AppComponent {
  title = 'Angular Demo';
}
