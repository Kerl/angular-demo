import { Component, OnInit } from '@angular/core';
import { BooksService } from 'app/books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books: any[];

  constructor(private booksService: BooksService) { }

  ngOnInit() {
    this.booksService.getBooks().then(books => this.books = books);
  }

}
